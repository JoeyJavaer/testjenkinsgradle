package com.zte9.crazyanswer;

/**
 * Created by 陈乐  on 2019/7/2.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class Constant {


    public static class SPParam {
        public static final String NOT_FIRST_LOGIN = "IsFirstLogin";
    }

    public static class UserParam {
        public static final String USER_1 = "15709310596";
        public static final String USER_2 = "13609344031";

        public static String userId    = "";
        public static String userToken = "";
        public static String userMac   = "";
    }

    public static class HttpParam {
        public static final String API_HOST = "http://111.11.189.9:8666";

        public static final String URL_VISIT_CLIENT  = "/gsydMusic_api/stat/music/visit";
        public static final String URL_EXIT_ACTIVITY = "/gsydMusic_pay/user/saveUserInfo";

        public static final String URL_VISIT_ACTIVITY    = "/singwithme_api/page/getpageCount";
        public static final String URL_ACTIVITY_USER     = "/singwithme_api/user/saveUser";
        public static final String URL_ACTIVITY_QUESTION = "/singwithme_api/question/getQuestionList";
        public static final String URL_ACTIVITY_ANSWER   = "/singwithme_api/question/recordAnswerInfo";
        public static final String URL_INTEGRAL_RECORD   = "singwithme_api/integralRecord/selIntegraltotals";


        public static final String CLIENT_VERSION      = "appVersion";
        public static final String CLIENT_MAC          = "mac";
        public static final String FROM_SOURCE         = "4";
        public static final String ACTIVITY_USER_TOKEN = "token";
        public static final String ACTIVITY_VERSION    = "appVersion";
        public static final String CONTENT_CODE        = "5a4b88e31dcde1f1b8f100d80651db79";

    }


}
