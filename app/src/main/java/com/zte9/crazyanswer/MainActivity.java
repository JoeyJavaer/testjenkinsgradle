package com.zte9.crazyanswer;

import android.content.Intent;
import android.net.Network;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;

import com.zte9.crazyanswer.activity.SettingActivity;
import com.zte9.crazyanswer.base.BaseActivity;
import com.zte9.crazyanswer.http.api.NetWorks;
import com.zte9.crazyanswer.http.callback.NetCallBack;
import com.zte9.crazyanswer.http.callback.NetCallBackResponse;
import com.zte9.crazyanswer.http.response.BaseResponse;
import com.zte9.crazyanswer.http.response.Response;
import com.zte9.crazyanswer.http.response.ResponseIntegral;
import com.zte9.crazyanswer.http.response.ResponseQuestionList;
import com.zte9.crazyanswer.util.LogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends BaseActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private Button        mBtnSetting;
    private Button        mBtnStart;
    private TextView      mTvLog;
    private Handler       mHandler;
    private TimeRunnable  mTimeRunnable;
    private StringBuilder mSb;


    @Override
    protected void loadData() {

    }


    @Override
    protected void initOthers() {

    }

    @Override
    protected void initView() {
        mBtnSetting = (Button) findViewById(R.id.btn_setting);
        mBtnStart = (Button) findViewById(R.id.btn_start);
        mTvLog = (TextView) findViewById(R.id.tv_log);

        mBtnSetting.setOnClickListener(v -> settingUserInfo());
        mBtnStart.setOnClickListener(v -> startAnswer());
    }

    private void requestHttp() {
        if (mTimeRunnable.useTime == 0) {
            NetWorks.visitClient(new NetCallBackResponse() {
                @Override
                public void onSuccess(Response baseResponse) {
                    mSb.append(" 进入大厅 成功 \n");
                    mTvLog.setText(mSb.toString());
                }
            });
        } else if (mTimeRunnable.useTime == 1) {
            visitActivity();
        } else if (mTimeRunnable.useTime == 2) {
            userLogin();
        } else if (mTimeRunnable.useTime == 3) {
            getQuestion();
        } else if (mTimeRunnable.useTime == 4 || mTimeRunnable.useTime == 5
                || mTimeRunnable.useTime == 6 || mTimeRunnable.useTime ==
                7 || mTimeRunnable.useTime == 8) {
            answerQuetion();
        } else if (mTimeRunnable.useTime == 9) {//活动规则
            NetWorks.visitLottery(new NetCallBack<BaseResponse>() {
                @Override
                public void onSuccess(BaseResponse baseResponse) {
                    mSb.append(" 访问积分兑换页面 成功 \n");
                    mTvLog.setText(mSb.toString());
                }
            });
        } else if (mTimeRunnable.useTime == 10) {//积分兑换
            NetWorks.visitRules(new NetCallBack<BaseResponse>() {
                @Override
                public void onSuccess(BaseResponse baseResponse) {
                    mSb.append(" 访问活动规则页面 成功 \n");
                    mTvLog.setText(mSb.toString());
                }
            });
        }


    }

    private void visitActivity() {
        NetWorks.visitHome(new NetCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {

                mSb.append(" 访问活动首页 成功 \n");
                mTvLog.setText(mSb.toString());
            }

        });
    }

    private void userLogin() {
        NetWorks.login(new NetCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                mSb.append(" 用户登录 成功 \n");
                mTvLog.setText(mSb.toString());
            }
        });
    }

    private void getQuestion() {
        NetWorks.getQuestion(new NetCallBack<ResponseQuestionList>() {
            @Override
            public void onSuccess(ResponseQuestionList responseQuestionList) {
                LogUtils.i(TAG, "getQuestion success:" + responseQuestionList.getData().getQuestionList().toString());
                mSb.append(" 获取题目 成功 \n");
                mTvLog.setText(mSb.toString());
                questionList.clear();
                questionList.addAll(responseQuestionList.getData().getQuestionList());

            }
        });
    }

    List<ResponseQuestionList.DataBean.QuestionListBean> questionList = new ArrayList<>();

    private void answerQuetion() {
        if (questionList == null) {
            return;
        }

        if (mTimeRunnable.useTime == 4) {//回答第一题
            answerQuestionIndex(1);
        } else if (mTimeRunnable.useTime == 5) {
            answerQuestionIndex(2);
        } else if (mTimeRunnable.useTime == 6) {
            answerQuestionIndex(3);
        } else if (mTimeRunnable.useTime == 7) {
            answerQuestionIndex(4);
        } else if (mTimeRunnable.useTime == 8) {
            answerQuestionIndex(5);
        }

    }

    private void answerQuestionIndex(int index) {
        LogUtils.i(TAG, "index:" + index);
        ResponseQuestionList.DataBean.QuestionListBean bean = null;
        try {

            bean = questionList.get(index - 1);
        } catch (Exception e) {

        }
        if (bean == null)
            return;
        LogUtils.i(TAG, "bean:" + bean.toString());
        NetWorks.answerQuestion(bean.getId(), index, new NetCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                LogUtils.i(TAG, "answerQuestionIndex success:" + index);
                mSb.append(" 做答第:" + index + "题 成功 ");
                mTvLog.setText(mSb.toString());

                NetWorks.getUserInteger(new NetCallBack<ResponseIntegral>() {
                    @Override
                    public void onSuccess(ResponseIntegral responseIntegral) {
                        int integralTotal = responseIntegral.getData().getIntegralTotal();
                        mSb.append("用户积分:"+integralTotal+"\n");
                        mTvLog.setText(mSb.toString());
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                LogUtils.i(TAG, "answerQuestionIndex error:" + e.toString());
            }
        });

        if (index==5){

        }
    }

    private void startAnswer() {

        mSb=new StringBuilder();
        if (mHandler == null) {
            mHandler = new Handler();
        }

        if (mTimeRunnable == null) {
            mTimeRunnable = new TimeRunnable(mHandler);
        } else {
            mTimeRunnable.useTime = 0;
        }
        mHandler.post(mTimeRunnable);





        /*Observable<BaseResponse> observable = NetWorks.service.visitClient();//进入大厅
        observable.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap((Func1<BaseResponse, Observable<BaseResponse>>) baseResponse -> {
                    return NetWorks.service.visitActivity(Constant.UserParam.userId, 1);//大体统计
                }).observeOn(Schedulers.io())
                .flatMap((Func1<BaseResponse, Observable<BaseResponse>>) s -> {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userId", Constant.UserParam.userId);
                    params.put("contentcode", Constant.HttpParam.CONTENT_CODE);
                    params.put("usertoken", Constant.HttpParam.ACTIVITY_USER_TOKEN);
                    params.put("mac", Constant.HttpParam.CLIENT_MAC);
                    params.put("version", Constant.HttpParam.ACTIVITY_VERSION);
                    params.put("fromsource", Constant.HttpParam.FROM_SOURCE);
                    LogUtils.i(TAG, "json:" + params.toString());
                    PostJsonBody jsonBody = PostJsonBody.create(params.toString());
                    return NetWorks.service.userLogin(jsonBody);//获取答题题目
                }).observeOn(Schedulers.io())
                .flatMap((Func1<BaseResponse, Observable<ResponseQuestionList>>) baseResponse -> {
                    Observable<ResponseQuestionList> questions = NetWorks.service.getQuestions(Constant.UserParam.userId);
                    LogUtils.i(TAG, "getQuestion:" + questions.toString());
                    return questions;
                })
                .subscribeOn(Schedulers.io())
                .flatMap((Func1<BaseResponse, Observable<BaseResponse>>) baseResponse -> {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userid", Constant.UserParam.userId);
                    //                        params.put("questionId", qusId + "");
                    //                        params.put("questionNum", qusNum + "");
                    params.put("isCorrect", "1");
                    return NetWorks.service.answerQuestion(params);
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe();*/


    }

    private void settingUserInfo() {
        startActivity(new Intent(this, SettingActivity.class));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    private class TimeRunnable implements Runnable {
        public  long    useTime = 0;
        private Handler mHandler;

        public TimeRunnable(Handler handler) {
            mHandler = handler;
        }

        public long getUseTime() {
            return useTime;
        }

        @Override
        public void run() {
            MainActivity.this.requestHttp();
            useTime++;
            LogUtils.i(TAG, "run usetime:" + useTime);
            int i = new Random().nextInt(10) + 5;
            mHandler.postDelayed(this, 1000 * i);//
        }
    }
}
