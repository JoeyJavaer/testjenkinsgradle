package com.zte9.crazyanswer.http.callback;






import com.zte9.crazyanswer.http.response.BaseResponse;

import rx.Observer;


/**
 * Created by  chenle
 * Description 请求回调，包括成功，失败
 * Others
 */

public abstract class NetCallBackAll<T extends BaseResponse> implements Observer<T>,INetCallbackAll<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        onError(e.toString());
    }

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }


}
