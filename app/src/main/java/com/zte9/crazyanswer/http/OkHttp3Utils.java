package com.zte9.crazyanswer.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zte9.crazyanswer.Constant;
import com.zte9.crazyanswer.base.BaseApp;
import com.zte9.crazyanswer.util.LogUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class OkHttp3Utils {

    private static OkHttpClient mOkHttpClient;

    //设置缓存目录

    private static long SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MiB

    /**
     * 获取OkHttpClient对象
     *
     * @return
     */
    public static OkHttpClient getOkHttpClient(String cacheFile) {
        Cache cache = new Cache(new File(cacheFile), SIZE_OF_CACHE);

        if (null == mOkHttpClient) {
            //同样okhttp3后也使用build设计模式
            mOkHttpClient = new OkHttpClient.Builder()
                    //设置一个自动管理cookies的管理器
                    //                .cookieJar(new CookiesManager())
                    //添加拦截器
                    .addInterceptor(CachingControlInterceptor.REWRITE_RESPONSE_INTERCEPTOR_OFFLINE)
                    .addInterceptor(chain -> {
                        Request oriRequest = chain.request();
                        Request modifiedRequest = oriRequest.newBuilder()
                                .addHeader("Content-Type", "application/json; charset=utf-8" )//addHeader
                                //headers.put("Content-Type", "application/json; charset=utf-8");
                                .addHeader("model", "EC6108V92")
                                .addHeader("version", "19")
                                .addHeader("release", "4.4.2")
                                .addHeader("mac", Constant.UserParam.userMac)
                                .addHeader("versionCode", "113")
                                .addHeader("versionName", "EC6108V92")
                                .addHeader("userId", Constant.UserParam.userId)
                                .method(oriRequest.method(), oriRequest.body()).build();
//                        LogUtils.i("http request:"+modifiedRequest.toString());
                        Response proceed = chain.proceed(modifiedRequest);
//                        LogUtils.i("http Response:"+proceed.toString());
                        return proceed;
                    })
                    //添加网络连接器
                    .addNetworkInterceptor(CachingControlInterceptor.REWRITE_RESPONSE_INTERCEPTOR)
                    //.addNetworkInterceptor(new CookiesInterceptor(MyApplication.getInstance().getApplicationContext()))
                    //设置请求读写的超时时间
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    //                .cache(cache)
                    .build();
        }
        return mOkHttpClient;
    }

    /**
     * 自动管理Cookies
     */
    private static class CookiesManager implements CookieJar {
        private final PersistentCookieStore cookieStore = new PersistentCookieStore(BaseApp.getContext());

        @Override
        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
            if (cookies != null && cookies.size() > 0) {
                for (Cookie item : cookies) {
                    cookieStore.add(url, item);
                }
            }
        }

        @Override
        public List<Cookie> loadForRequest(HttpUrl url) {
            List<Cookie> cookies = cookieStore.get(url);
            return cookies;
        }
    }

    /**
     * 判断网络是否可用
     *
     * @param context Context对象
     */
    public static Boolean isNetworkReachable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo current = cm.getActiveNetworkInfo();
        if (current == null) {
            return false;
        }
        return (current.isAvailable());
    }
}
