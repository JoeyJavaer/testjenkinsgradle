package com.zte9.crazyanswer.http.callback;

import android.util.Log;


import com.zte9.crazyanswer.http.response.BaseResponse;

import rx.Observer;


/**
 * Created by chenle
 * Description  请求成功后的回调，只处理成功
 * Others
 */

public abstract class NetCallBack<T extends BaseResponse> implements Observer<T>,INetCallback<T> {


    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

        Log.i("NetCallBack","onError message:"+e.toString());
    }

    @Override
    public void onNext(T t) {
        if (t.isSuccess()) {
            onSuccess(t);
        } else {
            Log.e("NetCallBack", t.toString());
        }
    }


}
