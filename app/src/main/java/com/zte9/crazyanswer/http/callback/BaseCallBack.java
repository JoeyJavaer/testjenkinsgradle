package com.zte9.crazyanswer.http.callback;


import com.zte9.crazyanswer.http.response.BaseResponse;

/**
 * Created by chenle
 * Description 网络请求成功后的回调 ，只是发送了请求不做任何操作
 * Others
 */

public class BaseCallBack<T extends BaseResponse> extends NetCallBack<T>{


    @Override
    public void onSuccess(T t) {

    }


}

