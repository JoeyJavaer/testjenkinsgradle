package com.zte9.crazyanswer.http.response;

/**
 * Created by 陈乐  on 2019/7/4.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class Response {
    private int     code;
    private String  message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
