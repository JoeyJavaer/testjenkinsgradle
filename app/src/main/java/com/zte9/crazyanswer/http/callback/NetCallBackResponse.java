package com.zte9.crazyanswer.http.callback;

import android.util.Log;

import com.zte9.crazyanswer.http.response.BaseResponse;
import com.zte9.crazyanswer.http.response.Response;

import rx.Observer;


/**
 * Created by chenle
 * Description  请求成功后的回调，只处理成功
 * Others
 */

public abstract class NetCallBackResponse implements Observer<Response>,INetCallback<Response> {


    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

        Log.i("NetCallBack","onError message:"+e.toString());
    }

    @Override
    public void onNext(Response t) {
        if (t.getCode()==0) {
            onSuccess(t);
        } else {
            Log.e("NetCallBack", t.toString());
        }
    }


}
