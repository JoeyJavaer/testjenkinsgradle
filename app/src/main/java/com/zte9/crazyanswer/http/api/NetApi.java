package com.zte9.crazyanswer.http.api;


import com.zte9.crazyanswer.Constant;
import com.zte9.crazyanswer.http.PostJsonBody;
import com.zte9.crazyanswer.http.response.BaseResponse;
import com.zte9.crazyanswer.http.response.Response;
import com.zte9.crazyanswer.http.response.ResponseIntegral;
import com.zte9.crazyanswer.http.response.ResponseQuestionList;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by 陈乐  on  2019/03/21.
 * Email chenle@zet9.com  网络请求的接口
 * Description
 * Others
 */

public interface NetApi {


    @GET(Constant.HttpParam.URL_INTEGRAL_RECORD)
    Observable<ResponseIntegral>getClientIntegral(@Query("userId")String userId);

    //进入大厅时保存用户
    @GET(Constant.HttpParam.URL_VISIT_CLIENT)
    Observable<Response> visitClient(@Header("userId") String userId);


    @GET(Constant.HttpParam.URL_VISIT_ACTIVITY)
    Observable<BaseResponse> visitActivity(@Query("userId") String userId, @Query("statisticsx") String statisticsx);


    @POST(Constant.HttpParam.URL_ACTIVITY_USER)
    Observable<BaseResponse> userLogin(@Body PostJsonBody body);

    @GET(Constant.HttpParam.URL_ACTIVITY_QUESTION)
    Observable<ResponseQuestionList> getQuestions(@Query("userId") String userId);


    @POST(Constant.HttpParam.URL_ACTIVITY_ANSWER)
    Observable<BaseResponse> answerQuestion(@Body PostJsonBody body);

    @FormUrlEncoded
    @POST(Constant.HttpParam.URL_EXIT_ACTIVITY)
    Observable<BaseResponse> exitActivity(@FieldMap Map<String, String> params);

    //进入大厅时保存用户
    //    @FormUrlEncoded
    //    @POST(HttpConstants.INSERT_USER)
    //    Observable<BaseResponse> insertUser(@FieldMap Map<String, String> params);
    //
    //
    //    //获取大厅升级版本
    //    @GET(HttpConstants.APP_VERSION_URL)
    //    Observable<ResponseAppVersion> getAppVersion(@Query("upgradeType") String updateType);


    //下载apk
    @Streaming
    @GET
    Observable<ResponseBody> downloadApk(@Url String url);


}
