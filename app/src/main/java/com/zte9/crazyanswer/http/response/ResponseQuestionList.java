package com.zte9.crazyanswer.http.response;

import java.util.List;

/**
 * Created by 陈乐  on 2019/7/3.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class ResponseQuestionList extends  BaseResponse{


    /**
     * data : {"questionList":[{"id":39,"question":"这首歌曲的演唱者或组合是?","optionA":"刘若英","optionB":"那英","optionC":"郭德纲","optionD":"岳云鹏","correctOption":"A","difficulty":1,"questionMusicUrl":"http://111.11.189.9:8666/media/6005934.mp3","questionPlayTime":22,"fullMusicId":"6005934","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":278,"question":"这段音乐出自哪首歌曲？","optionA":"伟大的渺小","optionB":"醉赤壁","optionC":"超越无限","optionD":"爱要怎么说出口","correctOption":"C","difficulty":1,"questionMusicUrl":"http://111.11.189.9:8666/media/7521276.mp3","questionPlayTime":18,"fullMusicId":"7521276","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":163,"question":"以上这段歌曲是出自\u201cCharcoal\u201d的哪一首歌？","optionA":"Natural Flow","optionB":"Oh Wow","optionC":"数星星","optionD":"Crafted Plan","correctOption":"C","difficulty":2,"questionMusicUrl":"http://111.11.189.9:8666/media/7409239.mp3","questionPlayTime":19,"fullMusicId":"7409239","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":295,"question":"这首歌曲出自《大唐荣耀》，哪位演员没有参演该剧？","optionA":"舒畅","optionB":"万茜","optionC":"周冬雨","optionD":"景甜","correctOption":"C","difficulty":2,"questionMusicUrl":"http://111.11.189.9:8666/media/7546022.mp3","questionPlayTime":21,"fullMusicId":"7546022","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":242,"question":"这首歌曲的名字是？","optionA":"唱只山歌给党听","optionB":"党的赞歌","optionC":"再唱山歌给党听","optionD":"赞歌","correctOption":"C","difficulty":3,"questionMusicUrl":"http://111.11.189.9:8666/media/7436832.mp3","questionPlayTime":22,"fullMusicId":"7436832","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null}],"playNum":1,"membershipType":0,"integralGain":null,"integralWeek":null,"integralExceed":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * questionList : [{"id":39,"question":"这首歌曲的演唱者或组合是?","optionA":"刘若英","optionB":"那英","optionC":"郭德纲","optionD":"岳云鹏","correctOption":"A","difficulty":1,"questionMusicUrl":"http://111.11.189.9:8666/media/6005934.mp3","questionPlayTime":22,"fullMusicId":"6005934","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":278,"question":"这段音乐出自哪首歌曲？","optionA":"伟大的渺小","optionB":"醉赤壁","optionC":"超越无限","optionD":"爱要怎么说出口","correctOption":"C","difficulty":1,"questionMusicUrl":"http://111.11.189.9:8666/media/7521276.mp3","questionPlayTime":18,"fullMusicId":"7521276","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":163,"question":"以上这段歌曲是出自\u201cCharcoal\u201d的哪一首歌？","optionA":"Natural Flow","optionB":"Oh Wow","optionC":"数星星","optionD":"Crafted Plan","correctOption":"C","difficulty":2,"questionMusicUrl":"http://111.11.189.9:8666/media/7409239.mp3","questionPlayTime":19,"fullMusicId":"7409239","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":295,"question":"这首歌曲出自《大唐荣耀》，哪位演员没有参演该剧？","optionA":"舒畅","optionB":"万茜","optionC":"周冬雨","optionD":"景甜","correctOption":"C","difficulty":2,"questionMusicUrl":"http://111.11.189.9:8666/media/7546022.mp3","questionPlayTime":21,"fullMusicId":"7546022","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null},{"id":242,"question":"这首歌曲的名字是？","optionA":"唱只山歌给党听","optionB":"党的赞歌","optionC":"再唱山歌给党听","optionD":"赞歌","correctOption":"C","difficulty":3,"questionMusicUrl":"http://111.11.189.9:8666/media/7436832.mp3","questionPlayTime":22,"fullMusicId":"7436832","channel":0,"createTime":1561684888000,"creator":"admin","modifiedTime":null,"modifier":null,"fullMusicInfoVO":null}]
         * playNum : 1
         * membershipType : 0
         * integralGain : null
         * integralWeek : null
         * integralExceed : null
         */

        private int playNum;
        private int                    membershipType;
        private Object                 integralGain;
        private Object                 integralWeek;
        private Object                 integralExceed;
        private List<QuestionListBean> questionList;

        public int getPlayNum() {
            return playNum;
        }

        public void setPlayNum(int playNum) {
            this.playNum = playNum;
        }

        public int getMembershipType() {
            return membershipType;
        }

        public void setMembershipType(int membershipType) {
            this.membershipType = membershipType;
        }

        public Object getIntegralGain() {
            return integralGain;
        }

        public void setIntegralGain(Object integralGain) {
            this.integralGain = integralGain;
        }

        public Object getIntegralWeek() {
            return integralWeek;
        }

        public void setIntegralWeek(Object integralWeek) {
            this.integralWeek = integralWeek;
        }

        public Object getIntegralExceed() {
            return integralExceed;
        }

        public void setIntegralExceed(Object integralExceed) {
            this.integralExceed = integralExceed;
        }

        public List<QuestionListBean> getQuestionList() {
            return questionList;
        }

        public void setQuestionList(List<QuestionListBean> questionList) {
            this.questionList = questionList;
        }

        public static class QuestionListBean {
            /**
             * id : 39
             * question : 这首歌曲的演唱者或组合是?
             * optionA : 刘若英
             * optionB : 那英
             * optionC : 郭德纲
             * optionD : 岳云鹏
             * correctOption : A
             * difficulty : 1
             * questionMusicUrl : http://111.11.189.9:8666/media/6005934.mp3
             * questionPlayTime : 22
             * fullMusicId : 6005934
             * channel : 0
             * createTime : 1561684888000
             * creator : admin
             * modifiedTime : null
             * modifier : null
             * fullMusicInfoVO : null
             */

            private int id;
            private String question;
            private String optionA;
            private String optionB;
            private String optionC;
            private String optionD;
            private String correctOption;
            private int    difficulty;
            private String questionMusicUrl;
            private int    questionPlayTime;
            private String fullMusicId;
            private int    channel;
            private long   createTime;
            private String creator;
            private Object modifiedTime;
            private Object modifier;
            private Object fullMusicInfoVO;

            @Override
            public String toString() {
                return "QuestionListBean{" +
                        "id=" + id +
                        ", question='" + question + '\'' +
                        ", optionA='" + optionA + '\'' +
                        ", optionB='" + optionB + '\'' +
                        ", optionC='" + optionC + '\'' +
                        ", optionD='" + optionD + '\'' +
                        ", correctOption='" + correctOption + '\'' +
                        ", difficulty=" + difficulty +
                        ", questionMusicUrl='" + questionMusicUrl + '\'' +
                        ", questionPlayTime=" + questionPlayTime +
                        ", fullMusicId='" + fullMusicId + '\'' +
                        ", channel=" + channel +
                        ", createTime=" + createTime +
                        ", creator='" + creator + '\'' +
                        ", modifiedTime=" + modifiedTime +
                        ", modifier=" + modifier +
                        ", fullMusicInfoVO=" + fullMusicInfoVO +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getOptionA() {
                return optionA;
            }

            public void setOptionA(String optionA) {
                this.optionA = optionA;
            }

            public String getOptionB() {
                return optionB;
            }

            public void setOptionB(String optionB) {
                this.optionB = optionB;
            }

            public String getOptionC() {
                return optionC;
            }

            public void setOptionC(String optionC) {
                this.optionC = optionC;
            }

            public String getOptionD() {
                return optionD;
            }

            public void setOptionD(String optionD) {
                this.optionD = optionD;
            }

            public String getCorrectOption() {
                return correctOption;
            }

            public void setCorrectOption(String correctOption) {
                this.correctOption = correctOption;
            }

            public int getDifficulty() {
                return difficulty;
            }

            public void setDifficulty(int difficulty) {
                this.difficulty = difficulty;
            }

            public String getQuestionMusicUrl() {
                return questionMusicUrl;
            }

            public void setQuestionMusicUrl(String questionMusicUrl) {
                this.questionMusicUrl = questionMusicUrl;
            }

            public int getQuestionPlayTime() {
                return questionPlayTime;
            }

            public void setQuestionPlayTime(int questionPlayTime) {
                this.questionPlayTime = questionPlayTime;
            }

            public String getFullMusicId() {
                return fullMusicId;
            }

            public void setFullMusicId(String fullMusicId) {
                this.fullMusicId = fullMusicId;
            }

            public int getChannel() {
                return channel;
            }

            public void setChannel(int channel) {
                this.channel = channel;
            }

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public String getCreator() {
                return creator;
            }

            public void setCreator(String creator) {
                this.creator = creator;
            }

            public Object getModifiedTime() {
                return modifiedTime;
            }

            public void setModifiedTime(Object modifiedTime) {
                this.modifiedTime = modifiedTime;
            }

            public Object getModifier() {
                return modifier;
            }

            public void setModifier(Object modifier) {
                this.modifier = modifier;
            }

            public Object getFullMusicInfoVO() {
                return fullMusicInfoVO;
            }

            public void setFullMusicInfoVO(Object fullMusicInfoVO) {
                this.fullMusicInfoVO = fullMusicInfoVO;
            }
        }
    }
}
