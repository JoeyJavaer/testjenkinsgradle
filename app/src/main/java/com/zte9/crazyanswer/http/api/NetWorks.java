package com.zte9.crazyanswer.http.api;


import com.google.gson.Gson;
import com.zte9.crazyanswer.Constant;
import com.zte9.crazyanswer.http.PostJsonBody;
import com.zte9.crazyanswer.http.RetrofitUtils;
import com.zte9.crazyanswer.http.callback.NetCallBack;
import com.zte9.crazyanswer.http.callback.NetCallBackAll;
import com.zte9.crazyanswer.http.callback.NetCallBackResponse;
import com.zte9.crazyanswer.http.response.BaseResponse;
import com.zte9.crazyanswer.http.response.Response;
import com.zte9.crazyanswer.http.response.ResponseIntegral;
import com.zte9.crazyanswer.http.response.ResponseQuestionList;
import com.zte9.crazyanswer.util.LogUtils;

import java.util.HashMap;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by 陈乐  on 2019/03/21.
 * Email chenle@zet9.com  网络请求的接口
 * Description
 * Others
 */
public class NetWorks extends RetrofitUtils {
    public static final String TAG="NetWorks";

    public static final NetApi service = RetrofitUtils.getRetrofit(Constant.HttpParam.API_HOST, "cache").create(NetApi.class);

    public static  void getUserInteger(NetCallBack<ResponseIntegral>callBack){
        applyScheduler(service.getClientIntegral(Constant.UserParam.userId),callBack);
    }

    public static void answerQuestion(int qusId, int qusNum, NetCallBack<BaseResponse> callBack) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", Constant.UserParam.userId);
        params.put("questionId", qusId + "");
        params.put("questionNum", qusNum + "");
        params.put("isCorrect", "1");
        Gson gson = new Gson();
        String s = gson.toJson(params);

        PostJsonBody jsonBody = PostJsonBody.create(s);
        applyScheduler(service.answerQuestion(jsonBody), callBack);

    }

    public static void getQuestion(NetCallBack<ResponseQuestionList> callBack) {

        applyScheduler(service.getQuestions(Constant.UserParam.userId), callBack);
    }

    //用户登录
    public static void login(NetCallBack<BaseResponse> callBack) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", Constant.UserParam.userId);
        params.put("contentcode", Constant.HttpParam.CONTENT_CODE);
        params.put("usertoken", Constant.HttpParam.ACTIVITY_USER_TOKEN);
        params.put("mac", Constant.HttpParam.CLIENT_MAC);
        params.put("version", Constant.HttpParam.ACTIVITY_VERSION);
        params.put("fromsource", Constant.HttpParam.FROM_SOURCE);
        String s1 = params.toString();

        Gson gson = new Gson();
        String s = gson.toJson(params);

        LogUtils.i(TAG,"login  s1:"+s1);
        LogUtils.i(TAG,"login  s:"+s);
        PostJsonBody jsonBody = PostJsonBody.create(s);
        applyScheduler(service.userLogin(jsonBody), callBack);
    }

    public static void visitLottery(NetCallBack<BaseResponse> callBack) {
        visitActivityPage(callBack, 9);
    }

    public static void visitRules(NetCallBack<BaseResponse> callBack) {
        visitActivityPage(callBack, 1);
    }

    public static void visitHome(NetCallBack<BaseResponse> callBack) {
        visitActivityPage(callBack, 1);
    }

    //统计进入活动首页
    public static void visitActivityPage(NetCallBack<BaseResponse> callBack, int pageNum) {
        applyScheduler(service.visitActivity(Constant.UserParam.userId, pageNum +""), callBack);
    }

    /**
     * 大厅打开时需要调用这个方法
     *
     * @param callBack 返回的处理
     */
    public static void visitClient(NetCallBackResponse callBack) {
        Observable<Response> observable = service.visitClient(Constant.UserParam.userId);
        observable.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.newThread())//子线程访问网络
                .observeOn(AndroidSchedulers.mainThread())//回调到主线程
                .subscribe(callBack);
    }


    /**
     * 添加公共参数
     *
     * @param params 公共参数
     */
    private static void setCommonParameter(HashMap<String, Object> params) {
        params.put("userId", Constant.UserParam.userId);
        params.put("userToken", Constant.UserParam.userToken);
    }


    /**
     * 插入观察者
     *
     * @param observable 返回
     * @param observer   处理
     * @param <T>        数据类型
     */
    private static <T> void setSubscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.newThread())//子线程访问网络
                .observeOn(AndroidSchedulers.mainThread())//回调到主线程
                .subscribe(observer);
    }

    /**
     * 网络请求的回调
     *
     * @param observable 被观察者
     * @param callBack   回调
     * @param <T>        参数
     */
    private static <T extends BaseResponse> void applyScheduler(Observable<T> observable, NetCallBack<T> callBack) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callBack);


    }



    private static <T extends BaseResponse> void applyScheduler(Observable<T> observable, NetCallBackAll<T> callBack) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callBack);
    }


}