package com.zte9.crazyanswer.http.response;

/**
 * Created by 陈乐  on 2019/7/4.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class ResponseIntegral extends BaseResponse {


    /**
     * data : {"id":395,"userId":"10086","integralDay":185,"integralWeek":185,"integralLastweek":0,"integralTotal":185,"createTime":1562211334000,"creator":"admin","modifiedTime":1562211351000,"modifier":"admin","list":null,"phone":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 395
         * userId : 10086
         * integralDay : 185
         * integralWeek : 185
         * integralLastweek : 0
         * integralTotal : 185
         * createTime : 1562211334000
         * creator : admin
         * modifiedTime : 1562211351000
         * modifier : admin
         * list : null
         * phone : null
         */

        private int id;
        private String userId;
        private int    integralDay;
        private int    integralWeek;
        private int    integralLastweek;
        private int    integralTotal;
        private long   createTime;
        private String creator;
        private long   modifiedTime;
        private String modifier;
        private Object list;
        private Object phone;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public int getIntegralDay() {
            return integralDay;
        }

        public void setIntegralDay(int integralDay) {
            this.integralDay = integralDay;
        }

        public int getIntegralWeek() {
            return integralWeek;
        }

        public void setIntegralWeek(int integralWeek) {
            this.integralWeek = integralWeek;
        }

        public int getIntegralLastweek() {
            return integralLastweek;
        }

        public void setIntegralLastweek(int integralLastweek) {
            this.integralLastweek = integralLastweek;
        }

        public int getIntegralTotal() {
            return integralTotal;
        }

        public void setIntegralTotal(int integralTotal) {
            this.integralTotal = integralTotal;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getCreator() {
            return creator;
        }

        public void setCreator(String creator) {
            this.creator = creator;
        }

        public long getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(long modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public String getModifier() {
            return modifier;
        }

        public void setModifier(String modifier) {
            this.modifier = modifier;
        }

        public Object getList() {
            return list;
        }

        public void setList(Object list) {
            this.list = list;
        }

        public Object getPhone() {
            return phone;
        }

        public void setPhone(Object phone) {
            this.phone = phone;
        }
    }
}
