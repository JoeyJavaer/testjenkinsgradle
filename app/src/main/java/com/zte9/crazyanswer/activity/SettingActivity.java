package com.zte9.crazyanswer.activity;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.zte9.crazyanswer.Constant;
import com.zte9.crazyanswer.R;
import com.zte9.crazyanswer.base.BaseActivity;
import com.zte9.crazyanswer.util.SpUtils;

public class SettingActivity extends BaseActivity {


    private Button   mBtnConfirm;
    private Button   mBtnCancel;
    private EditText mEtUserId;
    private EditText mEtUserToken;
    private EditText mEtMac;
    private Button   mBtnExchange;

    @Override
    protected void loadData() {
        String userId = Constant.UserParam.userId;
        String userToken = Constant.UserParam.userToken;
        String userMac = Constant.UserParam.userMac;
        mEtUserId.setHint(userId);
        mEtUserToken.setHint(userToken);
        mEtMac.setHint(userMac);
    }

    @Override
    protected void initOthers() {

        mBtnCancel.setOnClickListener(v -> finish());
        mBtnConfirm.setOnClickListener(v -> modifyUserInfo());
        mBtnExchange.setOnClickListener(v -> exchangerUser());
    }

    private void exchangerUser() {
        String userId = Constant.UserParam.userId;
        if (TextUtils.equals(Constant.UserParam.USER_1, userId)) {
            SpUtils.putString("userId", Constant.UserParam.USER_2);
            Constant.UserParam.userId = Constant.UserParam.USER_2;
            loadData();
        } else if (TextUtils.equals(Constant.UserParam.USER_2, userId)) {
            SpUtils.putString("userId", Constant.UserParam.USER_1);
            Constant.UserParam.userId = Constant.UserParam.USER_1;
            loadData();
        }
    }

    @Override
    protected void initView() {
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnExchange = (Button) findViewById(R.id.btn_exchange);

        mEtUserId = (EditText) findViewById(R.id.et_userId);
        mEtUserToken = (EditText) findViewById(R.id.et_userToken);
        mEtMac = (EditText) findViewById(R.id.et_mac);

    }

    private void modifyUserInfo() {
        String userId = mEtUserId.getText().toString().trim();
        String userToken = mEtUserToken.getText().toString().trim();
        String userMac = mEtMac.getText().toString().trim();

        if (!TextUtils.isEmpty(userId)) {
            SpUtils.putString("userId", userId);
            Constant.UserParam.userId = userId;
        }

        if (!TextUtils.isEmpty(userToken)) {
            SpUtils.putString("userToken", userToken);
            Constant.UserParam.userToken = SpUtils.getString("userToken");
        }

        if (!TextUtils.isEmpty(userMac)) {
            SpUtils.putString("userMac", userMac);
            Constant.UserParam.userMac = SpUtils.getString("userMac");
        }

    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_setting_activty;
    }
}
