package com.zte9.crazyanswer;

import com.zte9.crazyanswer.base.BaseApp;
import com.zte9.crazyanswer.util.UserInfoUtils;

/**
 * Created by 陈乐  on 2019/7/2.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class AppClient extends BaseApp {

    @Override
    public void onCreate() {
        super.onCreate();
        UserInfoUtils.init(this);
    }
}
