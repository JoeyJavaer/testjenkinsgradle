package com.zte9.crazyanswer.util;

import android.content.Context;

import com.zte9.crazyanswer.Constant;

/**
 * Created by 陈乐  on 2019/7/3.
 * Email chenle@zet9.com
 * Description
 * Others
 */
public class UserInfoUtils {
    public static void init(Context context) {
        if (SpUtils.getBoolean(Constant.SPParam.NOT_FIRST_LOGIN)) {//不是第一次登陆，只需要读取SP中的用户信息
            Constant.UserParam.userId = SpUtils.getString("userId");
            Constant.UserParam.userToken = SpUtils.getString("userToken");
            Constant.UserParam.userMac = SpUtils.getString("userMac");
            SpUtils.putBoolean(Constant.SPParam.NOT_FIRST_LOGIN, true);
        } else {//第一次登陆需要设置sp中的用户信息
            SpUtils.putString("userId", "15709310596");
//            SpUtils.putString("userId", "15709310598");
            Constant.UserParam.userId = SpUtils.getString("userId");
            // SpUtils.putString("userId", "13609344031");

            SpUtils.putString("userToken", "userToken");
            Constant.UserParam.userToken = SpUtils.getString("userToken");

            SpUtils.putString("userMac", "84:21:F1:74:76:F2");
            Constant.UserParam.userMac = SpUtils.getString("userMac");
        }

    }
}
